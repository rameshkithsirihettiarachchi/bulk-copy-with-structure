#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage:- bulkcopy /var/www/html/ < test.txt"
    exit 1
fi

prefix=$1
replace_me=""
filecount=1
IFS=$'\n'
for i in `cat $FILES_FOUND`
do
    relative=${i//$prefix/$replace_me}
    directory=$(dirname "${relative}")
    filename=$(basename "${relative}")
    mkdir -p $directory
    cp "$i" "./$directory"
done