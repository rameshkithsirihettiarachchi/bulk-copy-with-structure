# Bulk Copy With Structure

You can download multiple files with folder structure in one simple command by using this tool.

## Imagine

you have a file with file paths like below.

`list.txt`

```
var/www/html/myproject/application/controller/base.php
var/www/html/myproject/application/view/auth/login.php
var/www/html/myproject/application/view/panel/admin/table.php
var/www/html/myproject/application/view/panel/company/layout/footer.php
```

And your source directory's tree view:-

```
- application
    - controller
        - base.php
        - user.php
        - company.php
        - api
            - user.php
            - transaction.php
    - view
        -auth
            - login.php
            - signup.php
            - reset.php
        - admin
            - table.php
            - form.php
        - company
            - layout
                - footer.php
                - header.php
        - anonymous
```

And your expected result is like below:- 
```
- application
    - controller
        - base.php
    - view
        -auth
            - login.php
        - admin
            - table.php
        - company
            - layout
                - footer.php
```
Without this tool you want to do:-

1. Manually create each directory
2. Switch between source and destination files
3  Go to directories and sub directories
4. Copy and paste each files

But with this tool only you want to run
```
bulkcopy /var/www/html/myproject < list.txt
```

## Arguments

Required first variable as a directory. This directory is ignoring when coping files. If your filepath is `/a/b/c/` and you passed `/a/b/` as the first argument this tool will create `c` folder in the current directory.

Required a input file that not contain unusable lines and spaces.(Only file paths)